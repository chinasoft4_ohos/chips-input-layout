/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tylersuehr.chipexample.utils;

import ohos.agp.components.Component;

/**
 * KeyboardVisibilityEvent
 *
 * @since 2021-06-22
 */
public class KeyboardVisibilityEvent {
    public Component mRootView;

    //获取根界面进行传入
    public void setAbilitySliceRoot(Component RootView) {
        this.mRootView = RootView;
    }
}
