/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tylersuehr.chipexample.utils;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;

import java.lang.ref.WeakReference;

/**
 * @author Anoop S S
 * anoopvvs@gmail.com
 * on 28/02/2017
 */
public class SimpleUnregistrar implements Unregistrar {
    private WeakReference<AbilitySlice> activityWeakReference;
    private WeakReference<Component.LayoutRefreshedListener> onGlobalLayoutListenerWeakReference;

    public SimpleUnregistrar(AbilitySlice abilitySlice, Component.LayoutRefreshedListener globalLayoutListener) {
        activityWeakReference = new WeakReference(abilitySlice);
        onGlobalLayoutListenerWeakReference = new WeakReference(globalLayoutListener);
    }

    @Override
    public void unregister(Component rootview) {
        AbilitySlice abilitySlice=activityWeakReference.get();
        Component.LayoutRefreshedListener layoutRefreshedListener=onGlobalLayoutListenerWeakReference.get();
         if (null!=abilitySlice&&null!=layoutRefreshedListener){
             rootview.setLayoutRefreshedListener(null);
         }
        activityWeakReference.clear();
        onGlobalLayoutListenerWeakReference.clear();
    }
}
