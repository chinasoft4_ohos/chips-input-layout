/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tylersuehr.chipexample.slice;

import com.tylersuehr.chipexample.ResourceTable;
import com.tylersuehr.chipexample.utils.KeyboardVisibilityEvent;
import com.tylersuehr.chips.ChipDetailsView;
import com.tylersuehr.chips.FlowLayout;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;
import ohos.multimodalinput.event.KeyEvent;

/**
 * MainAbilitySlice
 *
 * @since 2021-06-22
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private StackLayout rootLayout;
    private FlowLayout mFirstFlowLayout;
    private Image mImage;
    private int id = 0;
    private Component component;
    private ChipDetailsView chipDetailsView;
    private Text mTextTitle;
    private Text mTextName;
    private Component view;
    private ScrollView mScrollView;
    private Component component2;
    private TextField mTextField;
    private KeyboardVisibilityEvent mKeyboardVisibilityEvent;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.BLUE.getValue());
        getWindow().setStatusBarColor(Color.getIntColor("#008BA3"));
        mScrollView = (ScrollView) findComponentById(ResourceTable.Id_id_scrollview);
        rootLayout = (StackLayout) findComponentById(ResourceTable.Id_root);
        rootLayout.setClickedListener(this);
        mFirstFlowLayout = (FlowLayout) findComponentById(ResourceTable.Id_id_first_flowlayout);
        mFirstFlowLayout.setListener(this);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(0, 0, 0));
        ComponentContainer.LayoutConfig scrollConfig = new ComponentContainer.LayoutConfig(
                DirectionalLayout.LayoutConfig.MATCH_PARENT, DirectionalLayout.LayoutConfig.MATCH_PARENT);
        mScrollView.setLayoutConfig(scrollConfig);
        mScrollView.enableScrollBar(1, true);
        rootLayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
            }
        });
        component2 = LayoutScatter.getInstance(MainAbilitySlice.this).parse(ResourceTable.Layout_layout_textField, null, false);
        mTextField = (TextField) component2.findComponentById(ResourceTable.Id_item_TextField);
        mTextField.setCursorElement(shapeElement);
        mFirstFlowLayout.addComponent(component2);

        mTextField.setKeyEventListener(new Component.KeyEventListener() {
            @Override
            public boolean onKeyEvent(Component components, KeyEvent keyEvent) {
                component = LayoutScatter.getInstance(MainAbilitySlice.this).parse(ResourceTable.Layout_layout_text, null, false);
                mTextTitle = (Text) component.findComponentById(ResourceTable.Id_txt_user_title);
                mTextName = (Text) component.findComponentById(ResourceTable.Id_txt_user_name);
                mImage = (Image) component.findComponentById(ResourceTable.Id_txt_user_fork);
                ShapeElement background = new ShapeElement();
                background.setCornerRadius(100);
                if (null == mTextField.getText().trim() || mTextField.getText().trim().equals("")) {
                    return true;
                }
                String num = (String) mTextField.getText().substring(0, 1);
                if (num.equals("a")) {
                    background.setRgbColor(new RgbColor(249, 164, 61));
                    mTextName.setTag(num);
                } else if (num.equals("n")) {
                    background.setRgbColor(new RgbColor(241, 99, 101));
                    mTextName.setTag(num);
                } else if (num.equals("k")) {
                    background.setRgbColor(new RgbColor(34, 147, 205));
                    mTextName.setTag(num);
                } else if (num.equals("y")) {
                    background.setRgbColor(new RgbColor(228, 198, 46));
                    mTextName.setTag(num);
                } else if (num.equals("w")) {
                    background.setRgbColor(new RgbColor(173, 98, 167));
                    mTextName.setTag(num);
                } else if (num.equals("z")) {
                    background.setRgbColor(new RgbColor(123, 113, 206));
                    mTextName.setTag(num);
                } else if (num.equals("p")) {
                    background.setRgbColor(new RgbColor(123, 113, 206));
                    mTextName.setTag(num);
                } else if (num.equals("i")) {
                    background.setRgbColor(new RgbColor(123, 113, 19));
                    mTextName.setTag(num);
                } else {
                    background.setRgbColor(new RgbColor((int) (Math.random() * 255),
                            (int) (Math.random() * 255), (int) (Math.random() * 255)));
                    mTextName.setTag(num);
                }
                mTextTitle.setBackground(background);

                mImage.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        mFirstFlowLayout.removeComponent((Component) component.getComponentParent());
                    }
                });
                if (keyEvent.isKeyDown() && keyEvent.getKeyCode() == keyEvent.KEY_ENTER) {
                    String s = mTextField.getText();
                    if (!"".equals(s)) {
                        mTextTitle.setText(s.substring(0, 1).toUpperCase());
                        mTextName.setText(s);
                        mTextField.setText("");
                        mFirstFlowLayout.addComponent(component);
                        mFirstFlowLayout.removeComponent(component2);
                        mFirstFlowLayout.addComponent(component2);
                    }
                }
                return false;
            }
        });
        mKeyboardVisibilityEvent = new KeyboardVisibilityEvent();
        mKeyboardVisibilityEvent.setAbilitySliceRoot(rootLayout);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_root:
                rootLayout.removeComponent(component);
                break;
            case ResourceTable.Id_button_delete:
                rootLayout.removeComponent(chipDetailsView);
                mFirstFlowLayout.removeComponent(view);
                break;
            case ResourceTable.Id_container:
                rootLayout.removeComponent(chipDetailsView);
                mFirstFlowLayout.postLayout();
                break;
            default:
                showDetailedChipView(component);
                view = component;
                break;
        }
    }

    private void showDetailedChipView(Component component) {
        Text text = (Text) component.findComponentById(ResourceTable.Id_txt_user_name);

        if (chipDetailsView != null) {
            rootLayout.removeComponent(chipDetailsView);
        }
        if (text.getTag() != null) {
            chipDetailsView = new ChipDetailsView(getContext(), text.getText(), (String) text.getTag());
            DirectionalLayout.LayoutConfig config = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT,
                    DirectionalLayout.LayoutConfig.MATCH_PARENT);
            if (component.getContentPositionX() > getDisplayWidthInPx(this) - 900) {
                config.setMargins(getDisplayWidthInPx(this) - 900, (int) component.getContentPositionY() + 200, 0, 0);
            } else {
                config.setMargins((int) component.getContentPositionX(), (int) component.getContentPositionY() + 200, 0, 0);
            }
            rootLayout.addComponent(chipDetailsView, config);
        }
        chipDetailsView.setOnDeleteClicked(this);
    }

    /**
     * 获取屏幕宽度
     *
     * @param context context
     * @return 屏幕宽度
     */
    public static int getDisplayWidthInPx(Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        return display.getAttributes().width;
    }
}
