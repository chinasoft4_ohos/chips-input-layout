# chips-input-layout

#### 项目介绍
- 项目名称：chips-input-layout
- 所属系列：openharmony的第三方组件适配移植
- 功能：允许用户在输入的时候过滤chip,允许用户输入自定义chip,可以指定chip是否显示详情，是否可以删除，是否有头像,可以指定chip是如何在memory中管理或者存储的,ChipsInputView对选中的chip进行验证,大多数view都是高度可自定义的
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release 2.3

#### 效果演示
![效果演示](./img/demo.gif)

#### 安装教程
                                
1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:chips-input-layout:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

使用该库非常简单，只需查看提供的示例的源代码。（查看ViewCreateHelper.java中在ScrollView和ListContainer中的使用）
```示例XML
    <ScrollView
            ohos:id="$+id:id_scrollview"
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:left_padding="10vp"
            ohos:right_padding="10vp">

            <DirectionalLayout
                ohos:height="match_parent"
                ohos:width="match_parent"
                ohos:orientation="vertical"
                ohos:top_margin="20vp">

                <com.tylersuehr.chips.FlowLayout
                    ohos:id="$+id:id_first_flowlayout"
                    ohos:height="match_parent"
                    ohos:width="match_parent"
                    ohos:right_margin="20vp"
                    app:flChildSpacing="20"
                    app:flChildSpacingForLastRow="align"
                    app:flRowSpacing="32.0f"/>

            </DirectionalLayout>
        </ScrollView>
```

```java
        mFirstFlowLayout = (FlowLayout) findComponentById(ResourceTable.Id_id_first_flowlayout);
        mFirstFlowLayout.setListener(this);
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息

 MIT
