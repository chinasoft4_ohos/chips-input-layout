/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tylersuehr.chips;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * FlowLayout
 *
 * @since 2021.07.06
 */
public class FlowLayout extends ComponentContainer implements Component.EstimateSizeListener, ComponentContainer.ArrangeListener {
    private static final String LOG_TAG = FlowLayout.class.getSimpleName();

    /**
     * Special value for the child view spacing.
     * SPACING_AUTO means that the actual spacing is calculated according to the size of the
     * container and the number of the child views, so that the child views are placed evenly in
     * the container.
     */
    public static final int SPACING_AUTO = -65536;

    /**
     * Special value for the horizontal spacing of the child views in the last row
     * SPACING_ALIGN means that the horizontal spacing of the child views in the last row keeps
     * the same with the spacing used in the row above. If there is only one row, this value is
     * ignored and the spacing will be calculated according to childSpacing.
     */
    public static final int SPACING_ALIGN = -65537;

    private static final int SPACING_UNDEFINED = -65538;

    private static final int UNSPECIFIED_GRAVITY = -1;

    private static final int ROW_VERTICAL_GRAVITY_AUTO = -65536;

    private static final boolean ISDEFAULT_FLOW = true;
    private static final int DEFAULT_CHILD_SPACING = 0;
    private static final int DEFAULT_CHILD_SPACING_FOR_LAST_ROW = SPACING_UNDEFINED;
    private static final double DEFAULT_ROW_SPACING = 0;
    private static final boolean ISDEFAULT_RTL = false;
    private static final int DEFAULT_MAX_ROWS = Integer.MAX_VALUE;

    private boolean isFlow = ISDEFAULT_FLOW;
    private int mChildSpacing = DEFAULT_CHILD_SPACING;
    private int mMinChildSpacing = DEFAULT_CHILD_SPACING;
    private int mChildSpacingForLastRow = DEFAULT_CHILD_SPACING_FOR_LAST_ROW;
    private double mRowSpacing = DEFAULT_ROW_SPACING;
    private double mAdjustedRowSpacing = DEFAULT_ROW_SPACING;
    private boolean isRtl = ISDEFAULT_RTL;
    private int mMaxRows = DEFAULT_MAX_ROWS;
    private int mGravity = UNSPECIFIED_GRAVITY;
    private int mRowVerticalGravity = ROW_VERTICAL_GRAVITY_AUTO;
    private String mRowVerticalGravityStr = null;
    private int mExactMeasuredHeight;

    private List<Float> mHorizontalSpacingForRow = new ArrayList<>();
    private List<Integer> mHeightForRow = new ArrayList<>();
    private List<Integer> mWidthForRow = new ArrayList<>();
    private List<Integer> mChildNumForRow = new ArrayList<>();
    private ClickedListener clickedListener;
    private int marginLeft = 0;
    private int marginTop = 0;
    private int marginBottom = 0;
    private int marginRight = 0;

    public FlowLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        // 是否是流式布局 flRtl "true" 流式布局，反之将所有子 view 放在一行中 flMaxRows
        if (attrs.getAttr("flFlow").isPresent()) {
            isFlow = attrs.getAttr("flFlow").get().getBoolValue();
        }

        // 以行数表示的FlowLayout的最大高度。
        if (attrs.getAttr("flMaxRows").isPresent()) {
            mMaxRows = attrs.getAttr("flMaxRows").get().getIntegerValue();
        }

        // ' true '用于从右到左布局子视图。' false '从左到右布局。默认值是' false '
        if (attrs.getAttr("flRtl").isPresent()) {
            isRtl = attrs.getAttr("flRtl").get().getBoolValue();
        }

        // 子 view 间距
        if (attrs.getAttr("flChildSpacing").isPresent()) {
            String childMarginStr = attrs.getAttr("flChildSpacing").get().getStringValue();
            if (childMarginStr.equals("align")) {
                mChildSpacing = SPACING_ALIGN;
            } else {
                mChildSpacing = Integer.parseInt(childMarginStr);
            }
        }

        // 行距
        if (attrs.getAttr("flRowSpacing").isPresent()) {
            this.setRowSpacing((double) attrs.getAttr("flRowSpacing").get().getFloatValue());
        }

        // 最后一行的布局样式
        if (attrs.getAttr("flChildSpacingForLastRow").isPresent()) {
            String mChildSpacingForLastRowStr = attrs.getAttr("flChildSpacingForLastRow").get().getStringValue();
            if (mChildSpacingForLastRowStr.equals("auto")) {
                mChildSpacingForLastRow = SPACING_AUTO;
            } else if (mChildSpacingForLastRowStr.equals("align")) {
                mChildSpacingForLastRow = SPACING_ALIGN;
            } else {
                mChildSpacingForLastRow = Integer.parseInt(mChildSpacingForLastRowStr);
            }
        }

        // 每一行垂直方向的布局方式（居中，居底部）
        if (attrs.getAttr("flRowVerticalGravity").isPresent()) {
            mRowVerticalGravityStr = attrs.getAttr("flRowVerticalGravity").get().getStringValue();
        }

        setEstimateSizeListener(this);
        setArrangeListener(this);
    }

    public void setListener(ClickedListener clickedListener) {
        this.clickedListener = clickedListener;
    }

    @Override
    public boolean onEstimateSize(int width, int height) {
        final int widthSize = EstimateSpec.getSize(width);
        final int widthMode = EstimateSpec.getMode(width);
        final int heightSize = EstimateSpec.getSize(height);
        final int heightMode = EstimateSpec.getMode(height);

        mHorizontalSpacingForRow.clear();
        mHeightForRow.clear();
        mWidthForRow.clear();
        mChildNumForRow.clear();

        int measuredHeight = 0, measuredWidth = 0, childCount = getChildCount();

        int rowWidth = 0, maxChildHeightInRow = 0, childNumInRow = 0;

        final int rowSize = widthSize - getPaddingLeft() - getPaddingRight();

        int rowTotalChildWidth = 0;
        final boolean isAllowFlow = widthMode != EstimateSpec.UNCONSTRAINT && isFlow;
        final int childSpacing = mChildSpacing == SPACING_ALIGN && widthMode == EstimateSpec.UNCONSTRAINT
                ? 0 : mChildSpacing;
        final double tmpSpacing = childSpacing == SPACING_ALIGN ? mMinChildSpacing : childSpacing;

        for (int i = 0; i < childCount; i++) {
            Component child = getComponentAt(i);
            child.setClickedListener(new ClickedListener() {
                @Override
                public void onClick(Component component) {
                    clickedListener.onClick(component);
                }
            });
            if (child.getVisibility() == HIDE) {
                continue;
            }

            LayoutConfig childParams = child.getLayoutConfig();
            int horizontalMargin = 0, verticalMargin = 0;
            horizontalMargin = childParams.getMarginLeft() + childParams.getMarginRight();
            verticalMargin = childParams.getMarginTop() + childParams.getMarginBottom();

            int childWidth = child.getEstimatedWidth();
            int childHeight = child.getEstimatedHeight();
            if (isAllowFlow && rowWidth + childWidth > rowSize) {
                mHorizontalSpacingForRow.add(
                        getSpacingForRow(childSpacing, rowSize, rowTotalChildWidth, childNumInRow));
                mChildNumForRow.add(childNumInRow);
                mHeightForRow.add(maxChildHeightInRow);
                mWidthForRow.add(rowWidth - (int) tmpSpacing);
                if (mHorizontalSpacingForRow.size() <= mMaxRows) {
                    measuredHeight += maxChildHeightInRow;
                }
                measuredWidth = Math.max(measuredWidth, rowWidth);

                // Place the child view to next row
                childNumInRow = 1;
                rowWidth = childWidth + (int) tmpSpacing;
                rowTotalChildWidth = childWidth;
                maxChildHeightInRow = childHeight;
            } else {
                childNumInRow++;
                rowWidth += childWidth + tmpSpacing;
                rowTotalChildWidth += childWidth;
                maxChildHeightInRow = Math.max(maxChildHeightInRow, childHeight);
            }
        }

        // Measure remaining child views in the last row
        if (mChildSpacingForLastRow == SPACING_ALIGN) {
            // For SPACING_ALIGN, use the same spacing from the row above if there is more than one
            // row.
            if (mHorizontalSpacingForRow.size() >= 1) {
                mHorizontalSpacingForRow.add(
                        mHorizontalSpacingForRow.get(mHorizontalSpacingForRow.size() - 1));
            } else {
                mHorizontalSpacingForRow.add(
                        getSpacingForRow(childSpacing, rowSize, rowTotalChildWidth, childNumInRow));
            }
        } else if (mChildSpacingForLastRow != SPACING_UNDEFINED) {
            // For SPACING_AUTO and specific DP values, apply them to the spacing strategy.
            mHorizontalSpacingForRow.add(
                    getSpacingForRow(mChildSpacingForLastRow, rowSize, rowTotalChildWidth, childNumInRow));
        } else {
            // For SPACING_UNDEFINED, apply childSpacing to the spacing strategy for the last row.
            mHorizontalSpacingForRow.add(
                    getSpacingForRow(childSpacing, rowSize, rowTotalChildWidth, childNumInRow));
        }

        mChildNumForRow.add(childNumInRow);
        mHeightForRow.add(maxChildHeightInRow);
        mWidthForRow.add(rowWidth - (int) tmpSpacing);
        if (mHorizontalSpacingForRow.size() <= mMaxRows) {
            measuredHeight += maxChildHeightInRow;
        }
        measuredWidth = Math.max(measuredWidth, rowWidth);

        if (childSpacing == SPACING_ALIGN) {
            measuredWidth = widthSize;
        } else if (widthMode == EstimateSpec.UNCONSTRAINT) {
            measuredWidth = measuredWidth + getPaddingLeft() + getPaddingRight();
        } else {
            measuredWidth = Math.min(measuredWidth + getPaddingLeft() + getPaddingRight(), widthSize);
        }

        measuredHeight += getPaddingTop() + getPaddingBottom();
        int rowNum = Math.min(mHorizontalSpacingForRow.size(), mMaxRows);
        double rowSpacing = mRowSpacing == SPACING_ALIGN && heightMode == EstimateSpec.UNCONSTRAINT
                ? 0 : mRowSpacing;
        if (rowSpacing == SPACING_ALIGN) {
            if (rowNum > 1) {
                mAdjustedRowSpacing = (heightSize - measuredHeight) / (rowNum - 1);
            } else {
                mAdjustedRowSpacing = 0;
            }
            measuredHeight = heightSize;
        } else {
            mAdjustedRowSpacing = rowSpacing;
            if (rowNum > 1) {
                measuredHeight = heightMode == EstimateSpec.UNCONSTRAINT
                        ? ((int) (measuredHeight + mAdjustedRowSpacing * (rowNum - 1)))
                        : (Math.min((int) (measuredHeight + mAdjustedRowSpacing * (rowNum - 1)),
                        heightSize));
            }
        }
        mExactMeasuredHeight = measuredHeight;

        measuredWidth = widthMode == EstimateSpec.PRECISE ? widthSize : measuredWidth;
        measuredHeight = heightMode == EstimateSpec.PRECISE ? heightSize : measuredHeight;
        System.out.println("measuredHeight" + measuredHeight);
        setEstimatedSize(measuredWidth, measuredHeight);
        return false;
    }

    @Override
    public boolean onArrange(int left, int top, int right, int bottom) {
        final int paddingLeft = getPaddingLeft(), paddingRight = getPaddingRight(),
                paddingTop = getPaddingTop(), paddingBottom = getPaddingBottom();

        int x = isRtl ? (getWidth() - paddingRight) : paddingLeft;
        int y = paddingTop;

        int verticalGravity = mGravity & LayoutAlignment.VERTICAL_CENTER;
        int horizontalGravity = mGravity & LayoutAlignment.LEFT;

        switch (verticalGravity) {
            case LayoutAlignment.VERTICAL_CENTER: {
                int offset = (bottom - top - paddingTop - paddingBottom - mExactMeasuredHeight) / 2;
                y += offset;
                break;
            }
            case LayoutAlignment.BOTTOM: {
                int offset = bottom - top - paddingTop - paddingBottom - mExactMeasuredHeight;
                y += offset;
                break;
            }
            default:
                break;
        }
        int horizontalPadding = paddingLeft + paddingRight, layoutWidth = right - left;
        x += getHorizontalGravityOffsetForRow(horizontalGravity, layoutWidth, horizontalPadding, 0);
        int verticalRowGravity = 0;
        if (mRowVerticalGravityStr != null && mRowVerticalGravityStr.length() > 0) {
            if (mRowVerticalGravityStr.equals("center")) {
                verticalRowGravity = LayoutAlignment.LEFT;
            } else if (mRowVerticalGravityStr.equals("bottom")) {
                verticalRowGravity = LayoutAlignment.BOTTOM;
            }
        }

        int rowCount = mChildNumForRow.size(), childIdx = 0;
        for (int row = 0; row < Math.min(rowCount, mMaxRows); row++) {
            int childNum = mChildNumForRow.get(row);
            int rowHeight = mHeightForRow.get(row);
            double spacing = mHorizontalSpacingForRow.get(row);
            for (int i = 0; i < childNum && childIdx < getChildCount(); ) {
                Component child = getComponentAt(childIdx++);
                if (child.getVisibility() == HIDE) {
                    continue;
                } else {
                    i++;
                }

                LayoutConfig childParams = child.getLayoutConfig();

                marginLeft = childParams.getMarginLeft();
                marginRight = childParams.getMarginRight();
                marginTop = childParams.getMarginTop();
                marginBottom = childParams.getMarginBottom();

                int childWidth = child.getEstimatedWidth();
                int childHeight = child.getEstimatedHeight();
                int tt = y + marginTop;
                if (verticalRowGravity == LayoutAlignment.BOTTOM) {
                    tt = y + rowHeight - marginBottom - childHeight;
                } else if (verticalRowGravity == LayoutAlignment.VERTICAL_CENTER) {
                    tt = y + marginTop + (rowHeight - marginTop - marginBottom - childHeight) / 2;
                }
                int bb = tt + childHeight;
                if (isRtl) {
                    int l1 = x - marginRight - childWidth;
                    int r1 = x - marginRight;
                    child.setComponentPosition(l1, tt, r1, bb);
                    x -= childWidth + spacing + marginLeft + marginRight;
                } else {
                    int l2 = x + marginLeft;
                    int r2 = x + marginLeft + childWidth;
                    child.setComponentPosition(l2, tt, r2, bb);
                    x += childWidth + spacing + marginLeft + marginRight;
                }
            }
            x = isRtl ? (getWidth() - paddingRight) : paddingLeft;
            x += getHorizontalGravityOffsetForRow(
                    horizontalGravity, layoutWidth, horizontalPadding, row + 1);
            y += rowHeight + mAdjustedRowSpacing;
        }

        for (int i = childIdx; i < getChildCount(); i++) {
            Component child = getComponentAt(i);
            if (child.getVisibility() == HIDE) {
                continue;
            }
            child.setComponentPosition(0, 0, 0, 0);
        }
        return false;
    }

    private int getHorizontalGravityOffsetForRow(int horizontalGravity, int parentWidth, int horizontalPadding, int row) {
        if (mChildSpacing == SPACING_ALIGN || row >= mWidthForRow.size()
                || row >= mChildNumForRow.size() || mChildNumForRow.get(row) <= 0) {
            return 0;
        }

        int offset = 0;
        switch (horizontalGravity) {
            case LayoutAlignment.HORIZONTAL_CENTER:
                offset = (parentWidth - horizontalPadding - mWidthForRow.get(row)) / 2;
                break;
            case LayoutAlignment.RIGHT:
                offset = parentWidth - horizontalPadding - mWidthForRow.get(row);
                break;
            default:
                break;
        }
        return offset;
    }

    /**
     * Returns whether to allow child views flow to next row when there is no enough space.
     *
     * @return Whether to flow child views to next row when there is no enough space.
     */
    public boolean isFlow() {
        return isFlow;
    }

    /**
     * Returns the horizontal spacing between child views of the last row.
     *
     * @return The spacing, either {@link FlowLayout#SPACING_AUTO},
     * {@link FlowLayout#SPACING_ALIGN}, or a fixed size in pixels
     */
    public int getChildSpacingForLastRow() {
        return mChildSpacingForLastRow;
    }

    /**
     * Sets the horizontal spacing between child views of the last row.
     *
     * @param childSpacingForLastRow The spacing, either {@link FlowLayout#SPACING_AUTO},
     *                               {@link FlowLayout#SPACING_ALIGN}, or a fixed size in pixels
     */
    public void setChildSpacingForLastRow(int childSpacingForLastRow) {
        mChildSpacingForLastRow = childSpacingForLastRow;
        postLayout();
    }

    /**
     * Sets the vertical spacing between rows in pixels. Use SPACING_AUTO to evenly place all rows
     * in vertical.
     *
     * @param rowSpacing 行间距
     */
    public void setRowSpacing(double rowSpacing) {
        mRowSpacing = rowSpacing;
        postLayout();
    }

    /**
     * Returns the maximum number of rows of the FlowLayout.
     *
     * @return The maximum number of rows.
     */
    public int getMaxRows() {
        return mMaxRows;
    }

    /**
     * Sets the height of the FlowLayout to be at most maxRows tall.
     *
     * @param maxRows The maximum number of rows.
     */
    public void setMaxRows(int maxRows) {
        mMaxRows = maxRows;
        postLayout();
    }

    public void setGravity(int gravity) {
        if (mGravity != gravity) {
            mGravity = gravity;
            postLayout();
        }
    }

    public void setRowVerticalGravity(int rowVerticalGravity) {
        if (mRowVerticalGravity != rowVerticalGravity) {
            mRowVerticalGravity = rowVerticalGravity;
            postLayout();
        }
    }

    public boolean isRtl() {
        return isRtl;
    }

    public int getMinChildSpacing() {
        return mMinChildSpacing;
    }

    public void setMinChildSpacing(int minChildSpacing) {
        this.mMinChildSpacing = minChildSpacing;
        postLayout();
    }

    public int getRowsCount() {
        return mChildNumForRow.size();
    }

    private float getSpacingForRow(int spacingAttribute, int rowSize, int usedSize, int childNum) {
        float spacing;
        if (spacingAttribute == SPACING_ALIGN) {
            if (childNum > 1) {
                spacing = (rowSize - usedSize) / (childNum - 1);
            } else {
                spacing = 0;
            }
        } else {
            spacing = spacingAttribute;
        }
        return spacing;
    }
}
