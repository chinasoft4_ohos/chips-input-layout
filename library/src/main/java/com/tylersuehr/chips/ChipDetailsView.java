/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tylersuehr.chips;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * Copyright © 2017 Tyler Suehr
 * <p>
 * This view displays the details of a chip (specified in Google Material Design Guide).
 *
 * @author Tyler Suehr
 * @version 1.0
 */
public class ChipDetailsView extends StackLayout implements ChipComponent {
    private ChipImageRenderer mImageRenderer;
    private Text mTitleView;
    private Text mText;
    private Text mLabelView;
    private Button mButtonDelete;
    private Image mAvatarView;
    private DependentLayout mContentLayout;
    private final StackLayout mheader;

    /**
     * ChipDetailsView
     *
     * @param context context
     * @param text text
     * @param tag tag
     */
    public ChipDetailsView(Context context, String text, String tag) {
        super(context);
        Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_chip_view_detailed, null, false);
        addComponent(component);
        mheader = (StackLayout) component.findComponentById(ResourceTable.Id_header);
        mContentLayout = (DependentLayout) component.findComponentById(ResourceTable.Id_container);
        mAvatarView = (Image) component.findComponentById(ResourceTable.Id_avatar);
        mText = (Text) component.findComponentById(ResourceTable.Id_text);
        mTitleView = (Text) component.findComponentById(ResourceTable.Id_title);
        mLabelView = (Text) component.findComponentById(ResourceTable.Id_subtitle);
        mButtonDelete = (Button) component.findComponentById(ResourceTable.Id_button_delete);
        // Hide the view on touch outside of it by making it focusable.
        mText.setText(text.substring(0, 1));
        mTitleView.setText(text);
        ShapeElement background = new ShapeElement();
        background.setCornerRadius(100);
        if ("0".equals(tag)) {
            background.setRgbColor(new RgbColor(249, 164, 61));
        } else if ("1".equals(tag)) {
            background.setRgbColor(new RgbColor(241, 99, 101));
        } else if ("2".equals(tag)) {
            background.setRgbColor(new RgbColor(34, 147, 205));
        } else if ("3".equals(tag)) {
            background.setRgbColor(new RgbColor(228, 198, 46));
        } else if ("4".equals(tag)) {
            background.setRgbColor(new RgbColor(173, 98, 167));
        } else if ("5".equals(tag)) {
            background.setRgbColor(new RgbColor(123, 113, 206));
        } else if ("6".equals(tag)) {
            background.setRgbColor(new RgbColor(123, 113, 206));
        } else if ("7".equals(tag)) {
            background.setRgbColor(new RgbColor(123, 113, 19));
        } else {
            background.setRgbColor(new RgbColor(21, 255, 236));
        }
        mheader.setBackground(background);
        setFocusable(FOCUS_ENABLE);
        setClickable(true);
    }

    @Override
    public void setChipOptions(ChipOptions options) {
        if (options.mDetailsChipBackgroundColor != null) {
        }

        if (options.mDetailsChipTextColor != null) {
            mTitleView.setTextColor(new Color(Color.getIntColor("#757575")));
            mLabelView.setTextColor(new Color(Color.getIntColor("#757575")));
        } else {
            mTitleView.setTextColor(Color.BLACK);
            mLabelView.setTextColor(Color.BLACK);
        }

        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromRgbaInt(255));
        // Set an available delete icon
        if (options.mChipDeleteIcon != null) {
            mButtonDelete.setBackground(shapeElement);
        } else {
            mTitleView.setTextColor(Color.BLACK);
            mLabelView.setTextColor(Color.BLACK);
        }

        mImageRenderer = options.mImageRenderer;
    }

    /**
     * Displays the information stored in the given chip object.
     *
     * @param chip {@link Chip}
     */
    public void inflateWithChip(Chip chip) {
        // Set the title and subtitle
        mTitleView.setText(chip.getTitle());
        if (chip.getSubtitle() == null) {
            mLabelView.setVisibility(HIDE);
        } else {
            mLabelView.setText(chip.getSubtitle());
        }

        // Set an available avatar
        if (mImageRenderer == null) {
            throw new NullPointerException("Image renderer must be set!");
        }
        mImageRenderer.renderAvatar(mAvatarView, chip);
    }

    public void setOnDeleteClicked(ClickedListener onClickListener) {
        mButtonDelete.setClickedListener(onClickListener);
        mContentLayout.setClickedListener(onClickListener);
    }
}