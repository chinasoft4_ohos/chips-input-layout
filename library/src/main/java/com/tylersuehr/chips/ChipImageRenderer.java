package com.tylersuehr.chips;

import ohos.agp.components.Image;

/**
 * Copyright © 2017 Tyler Suehr
 * <p>
 * Defines a renderer for chip avatar images.
 * <p>
 * Implementations of this can used to do things like using libraries,
 * such as Picasso or Glide, to load the image for you.
 *
 * @author Tyler Suehr
 * @version 1.0
 */
public interface ChipImageRenderer {
    void renderAvatar(Image imageView, Chip chip);
}