package com.tylersuehr.chips;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.NestedScrollView;
import ohos.app.Context;

/**
 * Copyright © 2017 Tyler Suehr
 * <p>
 * Subclass of {@link NestedScrollView} that allows a maximum height to be specified
 * such that this views height cannot exceed it.
 *
 * @author Tyler Suehr
 * @version 1.0
 */
public class MaxHeightScrollView extends NestedScrollView implements Component.EstimateSizeListener {

    public MaxHeightScrollView(Context c, AttrSet attrs, String defStyleAttr) {
        super(c, attrs, defStyleAttr);
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        int width = Component.EstimateSpec.getSize(widthEstimateConfig);
        int height = Component.EstimateSpec.getSize(heightEstimateConfig);
        setEstimatedSize(
                Component.EstimateSpec.getChildSizeWithMode(width, width, Component.EstimateSpec.NOT_EXCEED),
                Component.EstimateSpec.getChildSizeWithMode(height, height, Component.EstimateSpec.NOT_EXCEED));
        return true;
    }
}
