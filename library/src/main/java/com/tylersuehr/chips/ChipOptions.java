package com.tylersuehr.chips;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.StateElement;
import ohos.app.Context;

/**
 * Copyright © 2017 Tyler Suehr
 * <p>
 * Contains all the mutable properties for this library.
 *
 * @author Tyler Suehr
 * @version 1.0
 */
final class ChipOptions {
    Element mChipDeleteIcon;
    boolean mShowAvatar;
    boolean mShowDelete;
    StateElement mDetailsChipBackgroundColor;
    StateElement mDetailsChipTextColor;
    ChipImageRenderer mImageRenderer;

    ChipOptions(Context c, AttrSet attrSet, int defStyleAttr) {
        mImageRenderer = new DefaultImageRenderer();
    }
}
