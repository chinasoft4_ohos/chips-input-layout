package com.tylersuehr.chips;

import ohos.agp.components.Image;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Copyright © 2017 Tyler Suehr
 * <p>
 * Implementation of {@link ChipImageRenderer} that affords the default
 * way of rendering avatar images.
 * <p>
 * The default renderer does this:
 * (1) Try to load the avatar uri, or
 * (2) Try to load the avatar drawable, or
 * (3) Load a circular tile with a letter.
 *
 * @author Tyler Suehr
 * @version 1.0
 */
class DefaultImageRenderer implements ChipImageRenderer {
    /**
     * 超时时间
     */
    private static final int CONNECT_TIMEOUT = 5000;
    private static final int READ_TIMEOUT = 8000;

    @Override
    public void renderAvatar(Image imageView, Chip chip) {
        if (chip.getAvatarUri() != null) {
            imageView.setPixelMap(getBitmapFromUrl(chip.getAvatarUri().toString()));
        } else if (chip.getAvatarDrawable() != null) {
            imageView.setBackground(chip.getAvatarDrawable());
        } else {
        }
    }

    /**
     * 根据Url获取网络图片资源
     *
     * @param url 图片url
     * @return PixelMap
     */
    private PixelMap getBitmapFromUrl(String url) {
        try {
            URLConnection conn = new URL(url).openConnection();
            conn.setConnectTimeout(CONNECT_TIMEOUT);
            conn.setReadTimeout(READ_TIMEOUT);
            InputStream is = conn.getInputStream();
            ImageSource source = ImageSource.create(is, new ImageSource.SourceOptions());
            ImageSource.DecodingOptions options = new ImageSource.DecodingOptions();
            PixelMap pixelMap = source.createPixelmap(options);
            is.close();
            return pixelMap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}